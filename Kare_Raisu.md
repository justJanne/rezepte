# Kare Raisu

## Zutaten

| Menge  | Zutat                      |
| -----: | :------------------------- |
| 150 g  | Sojaschnetzel              |
| 1 l    | Gemüsebrühe                |
| 1–2 cm | Ingwer                     |
| 2 EL   | Reisessig                  |
| 8 EL   | Sojasauce                  |
| 1 TL   | Sirachasauce               |
| 1 EL   | Paprikapulver (geräuchert) |
| 1 TL   | Salz                       |
| ½ TL   | Pfeffer                    |
| 1 TL   | Kreuzkümmel                |
| 1 TL   | Honig                      |
| 4 EL   | Olivenöl                   |
| 450 g  | Zwiebeln                   |
| 250 g  | Karotten                   |
| 300 g  | Kartoffeln                 |
| 500 ml | Wasser                     |
| 3      | Currywürfel                |
| 1 TL   | Austernsauce               |

## Anleitung

1. Sojaschnetzel in kochender Gemüsebrühe 10 Min. aufweichen.  
   Anschließend abtropfen lassen und ausdrücken.
2. Ingwer, Gewürze und Reisessig in einem verschließbaren Behälter vermengen und 10 Min. ziehen lassen.
3. Sojasauce, Sirachasauce und Sojaschnetzel dazugeben und gut vermengen.  
   45 Min. ziehen lassen.
4. Zwiebeln fein schneiden.
5. Hälfte der Zwiebeln und die marinierten Sojaschnetzel anbraten.  
   Rest der Zwiebeln in einem Topf anbraten bis sie leicht gebräunt sind.
6. Kartoffeln und Karotten in mundgerechte Stücke schneiden und zusammen mit dem Wasser in den Topf geben. Aufkochen und 15 Min. köcheln lassen.
7. Currywürfel und Austernsauce in den Topf geben und pürieren.
   Sojaschnetzel in den Topf geben und vermengen.
8. Weitere 5 Min. köcheln lassen.
