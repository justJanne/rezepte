# Jägersoße

## Zutaten

| Menge   | Zutat                   |
| ------: | :---------------------- |
| 150 g   | Champignons             |
| 150 g   | Steinpilze              |
| 1       | Zwiebel                 |
| 50 ml   | Olivenöl                |
| 200 ml  | Gemüsebrühe             |
| 200 ml  | Sahne                   |
| 200 ml  | Cremé Fraîche Kräuter   |
| 4 EL    | Mehl                    |
|         | Salz                    |
|         | Pfeffer                 |

## Anleitung

1. Pilze säubern und in Scheiben schneiden. Zwiebel fein würfeln.
2. Zwiebeln in der Pfanne glasig anschwitzen. Pilze dazugeben und schmoren lassen.
3. Mit Sahne, Cremé Fraîche und Brühe ablöschen.  
   Einige Minuten einkochen lassen.
4. Eine Mehlschwitze ansetzen und einmischen.
5. Mit Salz und Pfeffer abschmecken.
