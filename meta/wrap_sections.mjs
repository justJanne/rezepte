#!/usr/bin/env node
import readline from "readline";

function groupChildren(blocks, parentLevel) {
  const result = [];
  while (blocks.length) {
    const block = blocks.shift();
    if (block.t == "Header") {
      const level = block.c[0];
      const id = block.c[1][0];

      if (parentLevel >= level) {
        blocks.unshift(block);
        return result;
      } else {
        const children = groupChildren(blocks, level);
        result.push({
          "t": "RawBlock",
          "c": [
            "html",
            "<div id=\"" + id + "\">"
          ]
        });
        result.push(block);
        result.push(...children);
        result.push({
          "t": "RawBlock",
          "c": [
            "html",
            "</div>"
          ]
        });
      }
    } else {
      result.push(block);
    }
  }
  return result;
}

const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
  terminal: false
});
rl.on('line', function (line) {
  const data = JSON.parse(line);
  const blocks = groupChildren(data.blocks, 0);
  console.log(JSON.stringify({
    ...data,
    blocks: blocks
  }));
});
