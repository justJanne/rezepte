# Italienischer Nudelauflauf

## Zutaten

### Soße

| Menge   | Zutat                   |
| ------: | :---------------------- |
| 2       | Paprikas                |
| 2       | Zwiebeln                |
| 50 ml   | Olivenöl                |
|         | Salz                    |
|         | Pfeffer                 |
| 150 g   | Feta                    |
| 2 EL    | Tomatenmark             |

### Nudeln

| Menge   | Zutat                   |
| ------: | :---------------------- |
| 500 g   | Nudeln                  |

### Garnitur

| Menge   | Zutat                   |
| ------: | :---------------------- |
|         | Pinienkerne             |
|         | Basilikum               |
|         | Mini-Mozzarella         |

## Anleitung

1. Backofen auf 220° Umluft vorheizen.
2. Pinienkerne rösten.
3. Paprikas und Zwiebeln würfeln und mit Olivenöl und Gewürzen würzen.  
   Für 30 Minuten auf 220° backen.
4. Das gebackene Gemüse mit dem Feta und dem Tomatenmark pürieren.
5. Nudeln nach Anleitung kochen.
6. Die Soße und die Nudeln sowie etwas Nudelwasser in einer Pfanne zusammengeben.  
   Mit den Pinienkernen, Basilikum und Mozzarella garnieren.
