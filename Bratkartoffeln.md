# Bratkartoffeln

## Zutaten

| Menge     | Zutat                    |
| --------: | :----------------------- |
| 300–400 g | Kartoffeln               |
| 100 g     | Zucker                   |
| 1         | Zwiebel                  |
| 1–2       | Eier                     |
| etwas     | Salz                     |
| etwas     | Petersilie               |
| etwas     | Cornichons / Agurkesalat |

## Anleitung

1. Kartoffeln schälen. In Scheiben (á ~4-6mm) schneiden.
   Scheiben für 5min in gesättigter Salzwasserlösung einweichen.
   In einem Sieb kurz abtropfen lassen.
2. In einem Topf gerade mit Wasser bedecken und mit 1 EL Salz aufsetzen.
   Sobald der Topf kocht runterstellen und 15min auf mittlerer Hitze köcheln
   lassen.
   Die Scheiben in einem Sieb wieder gut trocknen lassen.
3. In einer Fritteuse bei 190°C knusprig frittieren bis sie am Rand gerade eben
   goldbraun werden.
4. Zwiebeln fein würfeln und mit dem Speck auf kleiner bis mittlerer Hitze mit
   Deckel anbraten bis sie glasig werden.
   Dann auf hoher Hitze anbraten und Kartoffeln dazugeben.
5. Eier aufschlagen und mit Salz und Pfeffer rühren.
   Ei am Rand der Pfanne reingeben und so umrühren, dass ca. ¼–½ des
   Pfanneninhalts sich mit dem Ei verbindet.
6. Mit Gewürzgurken und Petersilie ganieren und servieren.
