# Kartoffellauchsuppe

## Zutaten

| Menge  | Zutat             |
| -----: | :---------------- |
| 500 g  | Kartoffeln        |
| 1      | Zwiebel           |
| 1 Bund | Frühlingszwiebeln |
| 2 EL   | Öl                |
| 750 ml | Gemüsebrühe       |
| 1 TL   | Pfeffer           |
| ½ TL   | Salz              |
| ½ TL   | Muskat            |
| 150 g  | Crème fraîche     |

## Anleitung

1. Kartoffeln und Zwiebeln schälen und würfeln.
2. Zwiebeln in 1 EL Öl in einem Topf andünsten
3. Kartoffeln und Brühe dazugeben.  
   Aufkochen und 15 Min. auf mittlerer Hitze köcheln lassen.
4. Frühlingszwiebeln putzen und waschen.  
   In feine Ringe schneiden und mit restlichem Öl leicht anbraten.
5. Crème fraîche zur Suppe geben und fein pürieren.
6. Frühlingszwiebeln und Gewürze dazugeben.
