# Butterplätzchen

## Zutaten

| Menge  | Zutat              |
| -----: | :----------------- |
| 250 g  | Butter             |
| 175 g  | Zucker             |
| 2 Pkg. | Vanillezucker      |
| 300 g  | Weizenmehl Typ 405 |
| ~1 EL  | Milch              |

## Anleitung

1. Backofen auf 160°C Umluft vorheizen.  
   Butter in einem Topf zerlassen und 45 Min. kalt stellen.
2. Die etwas fest gewordene Butter (sollte in etwa die Konsistenz von Knete haben) mit einem Mixer geschmeidig rühren.  
   Zucker und Vanillezucker in kleinen Portionen unterrühren.
3. Mehl und Milch in kleinen Portionen unterrühren.
4. Mindestens 30 Min. im Kühlschrank erkalten lassen.
5. Auf leicht bemehltem Tisch vorsichtig ausrollen und ausstechen.
6. Auf mittlerer Schiene 8–10 Min. backen.
