# Miso Ramen

## Zutaten

### Suppe

| Menge    | Zutat                     |
| -------: | :------------------------ |
| 2 Zehen  | Knoblauch                 |
| ca. 2 cm | Ingwer                    |
| 1        | Zwiebel                   |
| 1 EL     | Weißer Sesam (geröstet)   |
| 1 EL     | Sesamöl (geröstet)        |
| 113g     | Hack                      |
| 1 TL     | Doubanjiang               |
| 3 EL     | Miso Paste                |
| 1 EL     | Zucker                    |
| 1 EL     | Sake                      |
| 750 ml   | Brühe                     |
| 1 TL     | Salz                      |
| ½ TL     | Weißer Pfeffer (gemahlen) |

### Nudeln

| Menge | Zutat                     |
| ----: | :------------------------ |
| 180 g | Ramen Nudeln (getrocknet) |

### Ajitsuke Tamago

| Menge | Zutat     |
| ----: | :-------- |
| 2     | Eier      |
| 4 EL  | Sojasauce |
| 4 EL  | Mirin     |

### Shiraga Negi

| Menge    | Zutat |
| -------: | :---- |
| 1 Stange | Lauch |

## Anleitung

### Suppe

1. Knoblauch und Ingwer fein zerhacken oder zerreiben.  
   Zwiebel fein schneiden.  
   Sesamsamen grob zerstoßen.
2. Mit Sesamöl in einem großen Topf vorsichtig dünsten.
3. Hack hinzufügen und anbraten bis es gar ist.
4. Doubanjiang und Miso hinzufügen und schnell verrühren.  
   Sesamsamen und Zucker hinzufügen und erneut verrühren.
5. Mit Sake und Brühe ablöschen.  
   Aufkochen und köcheln lassen.
6. Mit Salz und weißem Pfeffer nachwürzen.  
   Mit Nudeln und Toppings servieren.  

### Ajitsu Tamago

1. In einem verschließbaren Gefrierbeutel Sojasauce, Mirin und Wasser vermengen.
2. Eier in gerade nicht mehr kochendem Wasser 7 Min.köcheln lassen.
3. 3 Min. in eiskaltem Wasser abschrecken und schälen.
4. Eier in den Gefrierbeutel legen.
5. 72 h im Kühlschrank marinieren lassen.

### Shiraga Negi

1. Grünen Teil vom Lauch abschneiden und ggf. anderweitig verwenden.  
   Weißen Teil putzen und waschen.  
   Innersten Teil vom Lauch entfernen und ggf. anderweitig verwenden.
2. Äußere weiße Schichten in feine Streifen schneiden.
3. 10 Min. in kaltem Wasser ziehen lassen.
