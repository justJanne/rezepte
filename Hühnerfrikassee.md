# Hühnerfrikassee

## Zutaten

| Menge    | Zutat                        |
| -------: | :--------------------------- |
| 1 l      | Gemüsebrühe                  |
| 180 g    | Hühnchenfilet                |
| 1        | Lorbeerblatt                 |
| 1 TL     | schwarzer Pfeffer (ganz)     |
| ½ TL     | schwarzer Pfeffer (gemahlen) |
| 3        | Nelken                       |
| ½ Dose   | Champignons                  |
| ½ Dose   | junge Erbsen & Möhrchen      |
| ½ Glas   | Spargelköpfe                 |
| 2 EL     | Butter                       |
| 3 EL     | Weizenmehl Typ 405           |
| 1 Schuss | Weißwein                     |
| 200 ml   | Sahne                        |
| 2 TL     | Salz                         |
| 1 Prise  | Muskat                       |
| 1 Schuss | Zitronensaft                 |

## Anleitung

1. Hühnchenfleisch in einem Topf mit Gemüsebrühe bedecken sowie Lorbeer, ganzer Pfeffer, Nelken und 1,5 TL Salz dazugeben.  
   45 Minuten köcheln lassen.
2. Währenddessen das Gemüse in mundgerechte Stücke schneiden.
3. Brühe durch ein Sieb vom Fleisch trennen und zusammen mit Gemüse für 8 Minuten garen.  
   Ausgesiebtes Fleisch in mundgerechte Stücke schneiden.
4. Brühe wieder aussieben und vom Gemüse trennen.
5. Butter im Topf erhitzen und mit Mehl zu einer hellen Mehlschwitze anschwitzen.  
   Nacheinander Wein, Brühe, Fleisch und Gemüse einrühren.  
   Aufkochen und 5 Minuten köcheln lassen.
6. Sahne einrühren und mit Gewürzen und Zitronensaft abschmecken.
