# Muzen & Berliner

## Zutaten

| Menge    | Zutat          |
| -------: | :------------- |
| 250 g    | Mehl           |
| 50 g     | Zucker         |
| 1 Prise  | Salz           |
| 125 ml   | lauwarme Milch |
| 1 Würfel | Hefe           |
| 30 g     | Butter         |
| 1        | Ei             |
| 1        | Eigelb         |
| ½        | Zitronenschale |

## Anleitung

1. Hefe, eine Prise Zucker und Milch mischen sodass die Hefe sich langsam auflöst.
2. Mehl, Salz und restlichen Zucker in einer Rührschüssel mischen und mit der aufgelösten Hefe vermengen.  
   Erneut gehen lassen.
3. Restliche Zutaten mit dem Teig vermengen und nochmals gehen lassen.
4. Bei ca. 180°C in Fett frittieren bis sie goldbraun sind.
