# Karamellsauce

## Zutaten

| Menge  | Zutat   |
| -----: | :------ |
| 300 g  | Zucker  |
| 100 ml | Wasser  |
| 150 g  | Butter  |
| 200 ml | Sahne   |
| 1 TL   | Salz    |
| 1 TL   | Vanille |

## Anleitung

1. Wasser und Zucker bei geringer Hitze in einem Topf vermengen bis sich der Zucker komplett aufgelöst hat.
2. Mischung auf hoher Hitze mit Deckel für ca. 2 Min. kochen. Danach ohne Deckel auf mittlerer Hitze köcheln lassen bis er am Rand braun wird (dabei nicht rühren).
3. Sobald das Karamell goldbraun wird leicht rühren bis die gesamte Mischung die gewünschte Karamellfarbe bekommt.
4. Topf vom Herd nehmen und die Butter und Sahne nacheinander einrühren.
5. Mit Salz und ggf. Vanille abschmecken.
