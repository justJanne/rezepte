# Poutine

## Zutaten

| Menge   | Zutat              |
| ------: | :----------------- |
| 2 EL    | Butter             |
| 1       | Zwiebel            |
| 3 Zehen | Knoblauch          |
| 2 EL    | Weizenmehl Typ 405 |
| 400 ml  | Gemüsebrühe        |
| 1 EL    | Sojasauce          |
| ½ TL    | Cayennepfeffer     |
| ½ TL    | Salz               |
| ½ TL    | schwarzen Pfeffer  |
| 2 EL    | Schnittlauch       |
| 125 g   | Mozzarella         |

## Anleitung

1. Zwiebel und Knoblauch in der Butter anschwitzen bis sie glasig sind.
2. Mehl hinzufügen und rösten bis es leicht braun ist.
3. Gemüsebrühe, Sojasauce und Cayennepfeffer hinzufügen.  
   Aufkochen und 5 Minuten köcheln lassen.
4. Mit Pfeffer und Salz abschmecken.
5. Mozzarella fein würfeln.
6. Mozzarella, Bratensauce und Schnittlauch nacheinander über Pommes geben, sodass der Mozzarella schmilzt.
