# Kartoffelsuppe

## Zutaten

| Menge  | Zutat        |
| -----: | :----------- |
| 3 EL   | Butter       |
| 1      | Zwiebel      |
| 1 Bund | Suppengrün   |
| 700 ml | Wasser       |
| 1 TL   | Salz         |
| 500 g  | Kartoffeln   |
| 1 TL   | Flüssigrauch |
| 1 EL   | Petersilie   |
| 200 g  | Wiener       |

## Anleitung

1. Kartoffeln, Zwiebeln und Suppengrün putzen, waschen und zerkleinern.  
   Würstchen in kleine Stücke schneiden.
2. Zwiebeln und Suppengrün in Butter kurz anschwitzen.
3. Mit Wasser ablöschen und abschmecken.
4. Aufkochen und 20 Min. köcheln lassen.
5. Etwa ⅓ pürieren und Würstchen dazugeben.
6. Weitere 10 Min. köcheln lassen.
