# Zitronenkuchen

## Zutaten

### Rührteig

| Menge   | Zutat                    |
| ------: | :----------------------- |
| 350 g   | Butter                   |
| 350 g   | Zucker                   |
| 2 Pkg.  | geriebene Zitronenschale |
| 5       | Eier                     |
| 275 g   | Weizenmehl               |
| 120 g   | Speisestärke             |
| 1 Pkg.  | Backpulver               |

### Glasur

| Menge   | Zutat        |
| ------: | :----------- |
| 350 g   | Puderzucker  |
| 7 EL    | Zitronensaft |

## Anleitung

1. Backofen auf 175° Umluft vorheizen.
2. Butter geschmeidig rühren.  
   Zucker und Zitronenschale unterrühren.  
   Eier einzeln unterrühren.
3. Mehl, Stärke und Backpulver mischen.  
   Langsam unterrühren.
4. Teig auf dem Backblech verteilen.  
   30 Minuten auf unterer Schiene backen.
5. Puderzucker mit gerade genug Zitronensaft verrühren, dass ein flüssiger Guss entsteht.  
   Auf dem noch heißen Kuchen gleichmäßig verteilen.
  
