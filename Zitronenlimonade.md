# Zitronenlimonade

## Zutaten

### Zuckersirup

| Menge  | Zutat  |
| -----: | :----- |
| 150 g  | Zucker |
| 500 ml | Wasser |

### Limonade

| Menge  | Zutat    |
| -----: | :------- |
| 4      | Zitronen |
| 1 l    | Wasser   |

## Anleitung

1. Zucker in 500ml Wasser in einem Topf auf dem Herd auflösen bis der Sirup komplett klar ist.
2. Zitronen teilen und auspressen.
3. Zuckersirup, Zitronensaft, und restliches Wasser mischen.
