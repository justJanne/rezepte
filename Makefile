ROOT_DIR:=$(shell dirname $(realpath $(firstword $(MAKEFILE_LIST))))
SOURCES := $(wildcard *.md)
OBJECTS := $(addprefix print/,$(SOURCES:%.md=%.pdf))

.PHONY: all
all: $(OBJECTS)

print/%.pdf: %.md meta/header.html meta/wrap_sections.mjs
	mkdir -p "$(@D)"
	pandoc "$<" -t html -H meta/header.html --filter meta/wrap_sections.mjs -o - | wkhtmltopdf -B 10mm -L 20mm -R 10mm -T 10mm --javascript-delay 0 - "$@"
