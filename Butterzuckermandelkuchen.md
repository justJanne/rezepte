# Butter-Zucker-Mandelkuchen

## Zutaten

### Teig

| Menge    | Zutat              |
| -------: | :----------------- |
| 1 Würfel | Hefe               |
| 125 ml   | lauwarme Milch     |
| 500 g    | Weizenmehl Typ 405 |
| 100 g    | Zucker             |
| 1 Prise  | Salz               |
| 50 g     | Butter             |

### Belag

| Menge    | Zutat              |
| -------: | :----------------- |
| 125 g    | Butter             |
| 75 g     | Zucker             |
| 1 Pkg.   | Vanillezucker      |
| 100 g    | Mandeln (gehobelt) |

## Anleitung

1. Backofen auf 180°C vorheizen.
2. Hefe und 1 TL Zucker in 5 EL Milch auflösen.
3. Hefe und Mehl vermengen und gehen lassen.
4. Zucker, Gewürze und weiche Butter mit dem Teig vermengen und gut durchkneten.
5. Teig auf ein mit Backpapier ausgelegtes Backblech streichen und erneut gehen lassen.
6. Für den Belag werden Butter und Zucker vermengt und auf den Teig gestrichen.  
   Die Mandeln werden vorsichtig in den Belag gedrückt.
7. 15 Min. auf mittlerer Schiene backen.
