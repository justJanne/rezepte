# Donauwellen

## Zutaten

### Rührteig

| Menge       | Zutat                       |
| ----------: | :-------------------------- |
| 700 g       | Sauerkirschen (Abtropfgew.) |
| 250 g       | Butter                      |
| 200 g       | Zucker                      |
| 1 Pkg.      | Vanillezucker               |
| 1 Prise     | Salz                        |
| 5           | Eier                        |
| 375 g       | Weizenmehl Typ 405          |
| 3 gestr. TL | Backpulver                  |
| 20 g        | Backkakao                   |
| 1 EL        | Milch                       |

### Buttercreme

| Menge  | Zutat                 |
| -----: | :-------------------- |
| 1 Pkg. | Puddingpulver Vanille |
| 100 g  | Zucker                |
| 500 ml | Milch                 |
| 250 g  | weiche Butter         |

### Guss

| Menge | Zutat                |
| ----: | :------------------- |
| 200 g | Zartbitterschokolade |
| 2 EL  | Sonnenblumenöl       |

## Anleitung

1. Backofen auf 160°C Umluft vorheizen.  
   Kirschen abtropfen lassen.
2. Butter mit einem Mixer (Rührstäbe) geschmeidig rühren.  
   Zucker, Vanillezucker und Salz hinzufügen bis die Masse sich bindet.  
   Eier einzeln für jeweils 30 Sek. Unterrühren.  
   Mehl und Backpulver in kleinen Portionen unterrühren.
3. Backblech einfetten und ⅔ des Teiges auf das Blech streichen.
4. Kakao und Milch unter den Rest des Teiges rühren und anschließend gleichmäßig auf den hellen Teig auf dem Blech verteilen.
5. Sauerkirschen kurz auf Küchenrolle trocken und leicht in den Teig drücken.
6. 40 Min. auf der unteren Schiene backen und abkühlen lassen.
7. Aus Puddingpulver, 100 g Zucker und Milch nach Anleitung einen Vanillepudding zubereiten und unter Luftabschluss erkalten lassen.
8. Weiche Butter mit dem Mixer (Rührstäbe) geschmeidig rühren.  
   Den erkalteten Pudding in kleinen Portionen unterrühren, dabei müssen Butter und Pudding Zimmertemperatur haben.
9. Puddingcreme auf den Teig streichen und mindestens eine Stunde im Kühlschrank weiter abkühlen lassen.
10. Zartbitterschokolade grob zerkleinern und zusammen mit dem Öl in einem Wasserbad oder einer Mikrowelle schmelzen lassen.  
   Den Guss auf die fest gewordene Buttercreme streichen und ggf. Mit einem Tortenganierkamm verzieren.
