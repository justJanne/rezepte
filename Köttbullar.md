# Köttbullar

## Zutaten

### Fleischbällchen

| Menge   | Zutat        |
| ------: | :----------- |
| 1       | Zwiebel      |
| 2 L     | Butter       |
| 50 g    | Semmelbrösel |
| 1 TL    | Salz         |
| ½ TL    | Piment       |
| 1 Prise | Muskat       |
| 300 g   | Hack         |

### Sauce

| Menge   | Zutat              |
| ------: | :----------------- |
| 4 EL    | Weizenmehl Typ 405 |
| 400 ml  | Gemüsebrühe        |
| 200 ml  | Sahne              |
| 2 TL    | Salz               |
| ½ TL    | schwarzer Pfeffer  |
| 1 Prise | Piment             |
| 3 EL    | Sojasauce          |

## Anleitung

1. Zwiebel fein würfeln, bei geringer Hitze in Butter anschwitzen bis sie leicht gebräunt sind.  
   Anschließend vom Herd nehmen und abkühlen lassen.
2. Zusammen mit Semmelbröseln und den Gewürzen zum veganen Hack geben und gut vermengen. Zu Bällchen mit je 25-30g Gewicht formen.  
   Im Kühlschrank für mindestens 10 Minuten ruhen lassen.
3. Die restliche Butter in eine heiße Pfanne geben, Fleischbällchen bei niedriger bis mittlerer Hitze 10-12 Minuten rundherum goldbraun braten.  
   Anschließend aus der Pfanne nehmen, abtropfen lassen und beiseite stellen.
4. Mehl zum Bratensatz in die Pfanne geben, 2 Minuten unter häufigem rühren braun rösten.  
   Mit Gemüsebrühe und Sahne ablöschen, mit Salz, Pfeffer, Piment und Sojasauce würzen.  
   Aufkochen und bei mittlerer Hitze 3-5 Minuten eindicken lassen.
5. Fleischbällchen zurück in die Sauce geben, kurz aufwärmen.

## Passt gut zu
- Kartoffelpüree
- Agurkesalat
