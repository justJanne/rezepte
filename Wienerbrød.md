# Wienerbrød

## Zutaten

### Teig

| Menge | Zutat                    |
| ----: | :----------------------- |
| 1     | Knack & Back Blätterteig |

### Füllung

| Menge | Zutat            |
| ----: | :--------------- |
| 30 g  | Butter           |
| 60 g  | Marzipanrohmasse |

### Glasur

| Menge | Zutat       |
| ----: | :---------- |
| 3 EL  | Puderzucker |
| 1 TL  | Wasser      |

### Belag

| Menge | Zutat      |
| ----: | :--------- |
| etwas | Zucker     |
| 100 g | Haselnüsse |
| 1     | Ei         |

## Anleitung

1. Backofen auf 180°C Umluft vorheizen.
2. Teig ausrollen und zu einer Fläche von ca. A4 Größe bringen.
3. Butter und Marzipan vermengen und in der Mitte auf den Teig legen.
4. Teig erst an den kurzen Seiten, dann an den langen zuklappen und mit einem verquirlten Ei bestreichen.
5. Haselnüsse und Zucker auf dem Teig verteilen.
6. 20–25 Min. auf mittlerer Schiene backen.  
   Mindestens 30 Min. abkühlen lassen.
7. Puderzucker und Wasser zu einer Glasur vermengen und auf dem Kuchen verteilen.
