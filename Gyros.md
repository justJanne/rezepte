# Gyros

## Zutaten

| Menge   | Zutat                   |
| ------: | :---------------------- |
| 180 g   | Hühnchenfleisch         |
| 1       | Zwiebel                 |
| 50 ml   | Olivenöl                |
| 1.5 TL  | Paprikapulver (edelsüß) |
| 1 TL    | Oregano (getrocknet)    |
| 1 TL    | Thymian (getrocknet)    |
| ½ TL    | Salz                    |
| ¼–½ TL  | Kreuzkümmel (gemahlen)  |
| 1 Prise | Pfeffer                 | 
| 1       | Knoblauchzehe           |
| ½ EL    | Zitronensaft            |
| 1 TL    | Senf                    |

## Anleitung

1. Olivenöl mit Gewürzen, Kräutern, Knoblauch, Zitronensaft und Senf vermengen.  
   Zwiebeln und Fleisch dazugeben.
2. Mindestens 3 Stunden marinieren lassen.

## Passt gut zu

- Pommes
- Kartoffelwedges
- Krautsalat
- Tomatenreis
- Pita
